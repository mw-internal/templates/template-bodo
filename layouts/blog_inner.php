<?php include template_dir() . "header.php"; ?>

<section class="grey-bg" id="blog">
    <div class="container">
        <div class="blog-page">
            <div class="module-posts-template-sophistika-item">

                <h3 class="title-small-center text-center">
                    <span class="edit" field="title" rel="content">Blog</span>
                </h3>
                <div class="edit post-content" field="content" rel="content">
                    <module data-type="pictures" data-template="default" rel="content" class="blog-main-slider"/>
                    <div class="edit" field="content_body" rel="content">
                        <div class="element">
                            <p align="justify">This text is set by default and is suitable for edit in real time. By default the drag and drop core feature will allow you to position it anywhere on
                                the site. Get creative, Make Web.</p>
                        </div>
                    </div>
                </div>
                <div class="edit" field="post-social-bar" rel="content">
                    <div class="mw-ui-col" style="padding-top: 12px; padding-right: 20px;">
                        <div style="background-color: #fff; padding: 0px 10px 6px 10px;">
                            <span class="mw-share"><span class="mw-icon-web-share"></span> Share Post</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="mw-ui-col">
                        <module type="facebook_like" show-faces="false" layout="box_count">
                    </div>
                </div>
            </div>
            <div class="module-posts-template-sophistika-item">
                <div class="edit" rel="content" field="comments">
                    <module data-type="comments" data-template="default" data-content-id="<?php print CONTENT_ID; ?>"/>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include template_dir() . "footer.php"; ?>
