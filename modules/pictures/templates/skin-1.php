<?php

/*

type: layout

name: Work Section

description: Skin 1

*/

?>
<div class="module-images-bodo">
    <?php if (is_array($data)): ?>
        <?php $rand = uniqid(); ?>
        <ul class="portfolio-image">
            <?php foreach ($data as $item): ?>
                <li class="col-md-6">
                    <a href="<?php print $item['filename'] ?>"><img alt="image" src="<?php print thumbnail($item['filename'], 600, 600, true); ?>">
                        <div class="decription-wrap"><?php ?>
                            <div class="image-bg">
                                <p class="desc"><?php print($item['title']); ?></p>
                            </div>
                        </div>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>

    <?php else : ?>
        <?php print lnotif('Click to add images.'); ?>
    <?php endif; ?>
</div>