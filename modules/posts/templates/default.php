<?php

/*

type: layout

name: Default (Bodo)

description: Bodo

*/
?>
<?php $rand = uniqid(); ?>


<?php


?>

<div class="clearfix module-posts-template-davy" id="posts-<?php print $rand; ?>">
    <?php if (!empty($data)): ?>
    <div class="grid">
        <?php
        $count = -1;
        foreach ($data as $item):
            $count++;
            ?>

            <div class="grid-item">
                <div class="wrap-article">
                    <?php if (!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
                    <a href="<?php print $item['link'] ?>">
                        <img alt="blog-1" class="img-circle text-center" src="<?php print $item['image']; ?>">
                    <?php endif; ?>
                    <?php if (!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                        <p class="subtitle fancy">
                            <span><?php print ($item['created_at']); ?></span>
                        </p>
                    <?php endif; ?>

                    <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                        <a href="<?php print $item['link'] ?>">
                            <h3 class="title">
                                <?php print $item['title'] ?>
                            </h3>
                        </a>
                    <?php endif; ?>

                    <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
                        <p class="content-blog"><?php print $item['description'] ?></p>
                    <?php endif; ?>
                    <?php if (!isset($show_fields) or $show_fields == false or in_array('read_more', $show_fields)): ?>
                        <a href="<?php print $item['link'] ?>" class="btn btn-default pull-fleft">
                            <?php $read_more_text ? print $read_more_text : print _e('Continue Reading', true); ?>
                            <i class="icon-chevron-right"></i></a>
                    <?php endif; ?>
                </div>
            </div>


        <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
