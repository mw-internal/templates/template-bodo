<?php

/*

type: layout

name: Default Bodo

description: Testimonials Bodo Default

*/

?>
<script>

$(document).ready(function(){
    $('.owl-carousel', '#<?php print $params['id']; ?>').owlCarousel({
      autoPlay: 3000, //Set AutoPlay to 3 seconds

      items : 1,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [979,1],
      itemsTablet : [768,1],
      itemsMobile : [479,1],

      // CSS Styles
      baseClass : "owl-carousel",
      theme : "owl-theme"
    });
})

</script>

<div class="mw-testimonials-bodo">
<?php $data = get_testimonials(); ?>
    <div class="owl-carousel">
	  <?php
        foreach($data as $item){
      ?>
        <div class="list-testimonial">
            <div class="content-testimonial">
                <?php if(isset($item['client_picture'])){ ?>
                <span class="mw-testimonials-item-image" style="background-image: url(<?php print $item['client_picture']; ?>);"></span>
                <?php } ?>
                <h3 class="testi">
                    “<?php print $item['content']; ?>”
                </h3>
                <p class="people">
                    <?php if(isset($item['client_website'])){ ?>
                       <a href="<?php print $item['client_website']; ?>" target="_blank">
                    <?php }?>
                    <?php print $item['name']; ?>
                    <?php if(isset($item['client_role'])) { print '&nbsp; - &nbsp; ' . $item['client_role']; } ?> <?php if(isset($item['client_company'])){ ?>&nbsp;at&nbsp;<?php print $item['client_company']; } ?>
                    <?php if(isset($item['client_website'])){ ?>
                       </a>
                    <?php }?>
                </p>
            </div>
        </div>
    <?php } ?>
    </div>
</div>