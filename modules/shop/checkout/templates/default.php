<?php

/*

type: layout

name: Default

description: Default cart template

*/

?>
<?php if ($requires_registration and is_logged() == false): ?>
    <module type="users/register"/>
<?php else: ?>
    <?php if ($payment_success == false): ?>

        <form class="mw-checkout-form" id="checkout_form_<?php print $params['id'] ?>" method="post"
              action="<?php print api_link('checkout') ?>">
            <?php $cart_show_enanbled = get_option('data-show-cart', $params['id']); ?>
            <?php if ($cart_show_enanbled != 'n'): ?>
                <br/>
                <module type="shop/cart" template="big" id="cart_checkout_<?php print $params['id'] ?>"
                        data-checkout-link-enabled="n"/>
            <?php endif; ?>
            <div class="mw-ui-row shipping-and-payment mw-shop-checkout-personal-info-holder">
                <div class="mw-ui-col" style="width: 33%;">
                    <div class="mw-ui-col-container">
                        <div class="well">
                            <?php $user = get_user(); ?>
                            <h2 style="margin-top:0 " class="edit nodrop" field="checkout_personal_inforomation_title"
                                rel="global" rel_id="<?php print $params['id'] ?>">
                                <?php _lang("Personal Information", "templates/bodo"); ?>
                            </h2>
                            <hr/>
                            <div class="mw-ui-field-holder"><label class="mw-ui-label">
                                    <?php _lang("First Name", "templates/bodo"); ?>
                                </label>
                                <input name="first_name" class="mw-ui-field" type="text"
                                       value="<?php if (isset($user['first_name'])) {
                                           print $user['first_name'];
                                       } ?>"/></div>
                            <div class="mw-ui-field-holder"><label class="mw-ui-label">
                                    <?php _lang("Last Name", "templates/bodo"); ?>
                                </label>
                                <input name="last_name" class="mw-ui-field" type="text"
                                       value="<?php if (isset($user['last_name'])) {
                                           print $user['last_name'];
                                       } ?>"/></div>
                            <div class="mw-ui-field-holder"><label class="mw-ui-label">
                                    <?php _lang("Email", "templates/bodo"); ?>
                                </label>
                                <input name="email" class="mw-ui-field" type="text"
                                       value="<?php if (isset($user['email'])) {
                                           print $user['email'];
                                       } ?>"/></div>
                            <div class="mw-ui-field-holder"><label class="mw-ui-label">
                                    <?php _lang("Phone", "templates/bodo"); ?>
                                </label>
                                <input name="phone" class="mw-ui-field" type="text"
                                       value="<?php if (isset($user['phone'])) {
                                           print $user['phone'];
                                       } ?>"/></div>
                        </div>
                    </div>
                </div>
                <?php if ($cart_show_shipping != 'n'): ?>
                    <div class="mw-ui-col mw-shop-checkout-shipping-holder">
                        <div class="mw-ui-col-container">
                            <module type="shop/shipping"/>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($cart_show_payments != 'n'): ?>
                    <div class="mw-ui-col">
                        <div class="mw-ui-col-container mw-shop-checkout-payments-holder">
                            <module type="shop/payments"/>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="alert hide"></div>
            <div class="mw-cart-action-holder">
                <hr/>


                <module type="shop/checkout/terms" />






                <?php $shop_page = get_content('is_shop=1'); ?>
                <?php if (is_array($shop_page)): ?>
                    <a href="<?php print page_link($shop_page[0]['id']); ?>" class="mw-ui-btn mw-template-btn-invert"
                       type="button">
                        <?php _lang("Continue Shopping", "templates/bodo"); ?>
                    </a>
                <?php endif; ?>

                <button class="mw-ui-btn mw-ui-btn-invert pull-right"
                        onclick="mw.cart.checkout('#checkout_form_<?php print $params['id'] ?>');"
                        type="button" id="complete_order_button" <?php if ($tems): ?> disabled="disabled"   <?php endif; ?>>
                    <?php _lang("Complete order", "templates/bodo"); ?>
                </button>

                <div class="clear"></div>
            </div>
        </form>
        <div class="mw-checkout-responce"></div>
    <?php else: ?>
        <h2>
            <?php _lang("Your payment was successfull.", "templates/bodo"); ?>
        </h2>
    <?php endif; ?>
<?php endif; ?>
