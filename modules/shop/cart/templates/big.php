<?php

/*

type: layout

name: Big

description: Full width cart template

*/

?>
<script>mw.moduleCSS("<?php print $config['url_to_module'] ?>templates/templates.css");</script>

<div class="mw-cart mw-cart-big mw-cart-<?php print $params['id'] ?> <?php print  $template_css_prefix; ?>">
    <div class="mw-cart-title mw-cart-<?php print $params['id'] ?>">
        <h2 class="edit page-title" rel="<?php print $params['id'] ?>" field="cart_title">
            Shopping Cart
        </h2>
    </div>
    <?php if (is_array($data)) : ?>
        <div class="table-responsive">
            <table class="table table-bordered mw-cart-table mw-cart-table-medium mw-cart-big-table">
                <thead>
                <tr>
                    <th align="center"><?php _lang("Image", "templates/bodo"); ?></th>
                    <th class="mw-cart-table-product"><?php _lang("Product Name", "templates/bodo"); ?></th>
                    <th align="center"><?php _lang("QTY", "templates/bodo"); ?></th>
                    <th><?php _lang("Price", "templates/bodo"); ?></th>
                    <th><?php _lang("Total", "templates/bodo"); ?></th>
                    <th align="center"><?php _lang("Delete", "templates/bodo"); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $total = cart_sum();
                foreach ($data as $item) :
                    //$total += $item['price']* $item['qty'];
                    ?>
                    <tr class="mw-cart-item mw-cart-item-<?php print $item['id'] ?>">
                        <td align="center"><?php if (isset($item['item_image']) and $item['item_image'] != false): ?>
                                <?php $p = $item['item_image']; ?>
                            <?php else: ?>
                                <?php $p = get_picture($item['rel_id']); ?>
                            <?php endif; ?>
                            <?php if ($p != false): ?>
                                <img height="70" class="img-polaroid img-rounded mw-order-item-image mw-order-item-image-<?php print $item['id']; ?>" src="<?php print thumbnail($p, 70, 70); ?>"/>
                            <?php endif; ?></td>
                        <td class="mw-cart-table-product"><?php print $item['title'] ?>
                            <?php if (isset($item['custom_fields'])): ?>
                                <?php print $item['custom_fields'] ?>
                            <?php endif ?></td>
                        <td align="center"><input type="number" min="1" class="input-mini form-control input-sm" value="<?php print $item['qty'] ?>" onchange="mw.cart.qty('<?php print $item['id'] ?>', this.value)"/></td>
                        <?php /* <td><?php print currency_format($item['price']); ?></td> */ ?>
                        <td class="mw-cart-table-price"><?php print currency_format($item['price']); ?></td>
                        <td class="mw-cart-table-price"><?php print currency_format($item['price'] * $item['qty']); ?></td>
                        <td align="center"><a title="<?php _lang("Remove", "templates/bodo"); ?>" class="mw-icon-close-round" href="javascript:mw.cart.remove('<?php print $item['id'] ?>');"></a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php $shipping_options = mw('shop\shipping\shipping_api')->get_active(); ?>
        <?php

        $show_shipping_info = get_option('show_shipping', $params['id']);

        if ($show_shipping_info === false or $show_shipping_info == 'y') {
            $show_shipping_stuff = true;
        } else {
            $show_shipping_stuff = false;
        }

        if (is_array($shipping_options)) :?>
            <div>
                <h3>
                    <?php _lang("Order summary", "templates/bodo"); ?>
                </h3>
                <div class="table-responsive">
                    <table cellspacing="0" cellpadding="0" class="table table-bordered  mw-cart-table mw-cart-table-medium checkout-total-table" width="100%">
                        <style scoped="scoped">


                            .checkout-total-table {
                                table-layout: fixed;
                            }

                            .checkout-total-table label {
                                display: block;
                                text-align: right;
                            }

                            .cell-shipping-total, .cell-shipping-price {
                                text-align: right;
                            }

                            .total_cost {
                                font-weight: normal;
                            }

                        </style>

                        <tbody>
                        <tr <?php if (!$show_shipping_stuff) : ?> style="display:none" <?php endif; ?>>
                            <td class="cell-shipping-country" colspan="2"><label>
                                    <?php _lang("Shipping to", "templates/bodo"); ?>
                                    :</label></td>
                            <td class="cell-shipping-country">
                                <module type="shop/shipping" view="select"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><label>
                                    <?php _lang("Shipping price", "templates/bodo"); ?>
                                    :</label></td>
                            <td class="cell-shipping-price">
                                <div class="mw-big-cart-shipping-price" style="display:inline-block">
                                    <module type="shop/shipping" view="cost"/>
                                </div>
                            </td>
                        </tr>


                        <?php if (function_exists('cart_get_tax') and get_option('enable_taxes', 'shop') == 1) { ?>

                            <tr>
                                <td colspan="2"><label>
                                        <?php _lang("Tax", "templates/bodo"); ?>
                                        :</label></td>
                                <td class="cell-shipping-price"><?php print currency_format(cart_get_tax()); ?></td>
                            </tr>

                        <?php } ?>


                        <tr>
                            <td colspan="2"><label>
                                    <?php _lang("Total Price", "templates/bodo"); ?>
                                    :</label></td>
                            <td class="cell-shipping-total">
                                <?php
                                $print_total = cart_total();

                                ?>
                                <span class="total_cost"><?php print currency_format($print_total); ?></span>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endif; ?>
        <?php
        if (!isset($params['checkout-link-enabled'])) {
            $checkout_link_enanbled = get_option('data-checkout-link-enabled', $params['id']);
        } else {
            $checkout_link_enanbled = $params['checkout-link-enabled'];
        }
        ?>
        <?php if ($checkout_link_enanbled != 'n') : ?>
            <?php $checkout_page = get_option('data-checkout-page', $params['id']); ?>
            <?php if ($checkout_page != false and strtolower($checkout_page) != 'default' and intval($checkout_page) > 0) {
                $checkout_page_link = content_link($checkout_page) . '/view:checkout';
            } else {
                $checkout_page_link = site_url('checkout');
            }
            ?>
            <a class="btn  btn-warning pull-right" href="<?php print $checkout_page_link; ?>">
                <?php _lang("Checkout", "templates/bodo"); ?>
            </a>
        <?php endif; ?>
    <?php else : ?>
        <h4 class="alert alert-warning">
            <?php _lang("Your cart is empty.", "templates/bodo"); ?>
        </h4>
    <?php endif; ?>
</div>
