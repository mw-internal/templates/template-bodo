<?php

/*

type: layout

name: Services Section

position: 4

*/
?>

<div class="safe-mode edit" field="layout-skin-4-<?php print $params['id'] ?>" rel="module">
    <section class="white-bg nodrop" id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="title-small">
                        <span class="safe-element">Services</span>
                    </h3>
                    <div class=" allow-drop">
                        <p class="content-detail">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        </p>
                    </div>
                </div>

                <div class="col-md-9 content-right">
                    <div class="row">
                        <ul class="listing-item cloneable">
                            <li class="cloneable">
                                <div class="col-md-4 col-sm-4">
                                    <i class="mdi mdi-trending-up mdi-24px"></i>
                                    <p class="head-sm">
                                        MARKETING
                                    </p>
                                    <p class="text-grey">
                                        Typi non habent claritatem insitam est usus legentis in iis qui facit eorum
                                        claritatem.
                                    </p>
                                </div>
                            </li>

                            <li class="cloneable">
                                <div class="col-md-4 col-sm-4">
                                    <i class="mdi mdi-gift-outline mdi-24px"></i>
                                    <p class="head-sm">
                                        WEB DESIGN
                                    </p>
                                    <p class="text-grey">
                                        Typi non habent claritatem insitam est usus legentis in iis qui facit eorum
                                        claritatem.
                                    </p>
                                </div>
                            </li>

                            <li class="cloneable">
                                <div class="col-md-4 col-sm-4">
                                    <i class="mdi mdi-grid-large mdi-24px"></i>
                                    <p class="head-sm">
                                        UI
                                    </p>
                                    <p class="text-grey">
                                        Typi non habent claritatem insitam est usus legentis in iis qui facit eorum
                                        claritatem.
                                    </p>
                                </div>
                            </li>
                        </ul>

                        <ul class="listing-item cloneable">
                            <li class="cloneable">
                                <div class="col-md-4 col-sm-4">
                                    <i class="mdi mdi-headphones mdi-24px"></i>
                                    <p class="head-sm">
                                        DIRECTOR
                                    </p>
                                    <p class="text-grey">
                                        Typi non habent claritatem insitam est usus legentis in iis qui facit eorum
                                        claritatem.
                                    </p>
                                </div>
                            </li>

                            <li class="cloneable">
                                <div class="col-md-4 col-sm-4">
                                    <i class="mdi mdi-magnet mdi-24px"></i>
                                    <p class="head-sm">
                                        UX DESIGN
                                    </p>
                                    <p class="text-grey">
                                        Typi non habent claritatem insitam est usus legentis in iis qui facit eorum
                                        claritatem.
                                    </p>
                                </div>
                            </li>

                            <li class="cloneable">
                                <div class="col-md-4 col-sm-4">
                                    <i class="mdi mdi-rocket-outline mdi-24px"></i>
                                    <p class="head-sm">
                                        FRONT END
                                    </p>
                                    <p class="text-grey">
                                        Typi non habent claritatem insitam est usus legentis in iis qui facit eorum
                                        claritatem.
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>