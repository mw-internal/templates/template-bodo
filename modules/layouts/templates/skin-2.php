<?php

/*

type: layout

name: Work Section

position: 2

*/
?>

<div class="safe-mode edit" field="layout-skin-2-<?php print $params['id'] ?>" rel="module">
    <section class="grey-bg mar-tm-10" id="work">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="title-small">
                        <span class="safe-element">Work</span>
                    </h3>
                    <p class="content-detail allow-drop">
                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. <br><br>Duis autem vel eum iriure dolor in
                        hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros.
                    </p>
                </div>
                <div class="col-md-9 content-right">
                    <module type="pictures" template="skin-1">
                </div>
            </div>
        </div>
    </section>
</div>