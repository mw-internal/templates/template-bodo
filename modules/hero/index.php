<?php

$file = get_option('file', $params['id']);

$alpha = get_option('alpha', $params['id']);

if($alpha == false){
    $alpha = 0;
    if(isset($params['alpha'])){
        $alpha = $params['alpha'];
    }
}




if ($file == false or $file == '') {
    if(isset($params['file'])){
        $file = $params['file'];
    }
    else{
        $file = '';
    }
}



    $paralax = get_option('paralax', $params['id']);

    if($paralax === false){
        if(isset($params['paralax'])){
            $paralax = $params['paralax'] == 'true';
        }
    }

    if($paralax === NULL){
        $paralax = false;
    }




    $disabled = $paralax ? '' : 'paralax-disabled';



 ?>
  <script>
     mw.moduleCSS("<?php print $config['url_to_module']; ?>style.css");
  </script>
  <script>
     mw.moduleJS("<?php print $config['url_to_module']; ?>script.js");
  </script>
<div class="mw-hero mw-paralax">
    <div class="mw-paralax-overlay" style="opacity:<?php print $alpha;  ?>"></div>
    <?php if ($file == '') {
       
    } else { ?>
           <?php if (preg_match('/(\.jpg|\.png|\.bmp|\.gif|\.jpeg)$/', $file)) { ?>
                <span style="background-image: url(<?php print $file; ?>);" class="mw-paralax-image <?php print $disabled; ?>" ></span>
           <?php } else{ ?>
                <video autoplay="true" loop="true" src="<?php print $file; ?>" class="mw-paralax-image <?php print $disabled; ?>" muted></video>
           <?php } ?>
    <?php } ?>

</div>