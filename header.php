<!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#">
<head>
    <title>{content_meta_title}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta property="og:title" content="{content_meta_title}"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="alternate" type="application/rss+xml" title="{og_site_name}" href="<?php print site_url('rss'); ?>"/>

    <!--styles-->
    <link href="<?php print template_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/owl.carousel.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/owl.theme.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/magnific-popup.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/responsive.css" rel="stylesheet">

    <!--fonts google-->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php print template_url(); ?>js/html5shiv.min.js"></script>
    <![endif]-->

    <script>
        mw.require('icon_selector.js');

        mw.iconLoader()
            .addIconSet('iconsMindLine')
            .addIconSet('iconsMindSolid')
            .addIconSet('materialDesignIcons')
            .addIconSet('mwIcons')
            .addIconSet('materialIcons');
    </script>

    <script>
        AddToCartModalContent = window.AddToCartModalContent || function (title) {
                var html = ''

                    + '<section class="text-center">'

                    + '<span class="sm-icon-bag2"></span>'

                    + '<h5>' + title + '</h5>'

                    + '<p class="m-t-10"><?php _lang("has been added to your cart", "templates/bodo"); ?></p>'

                    + '<a href="javascript:;" onclick="mw.tools.modal.remove(\'#AddToCartModal\')" class="mw-ui-btn m-10"><?php _lang("Continue shopping", "templates/bodo"); ?></a> '

                    + '<a href="<?php print checkout_url(); ?>" class="mw-ui-btn mw-ui-btn-invert m-10"><?php _lang("Checkout", "templates/bodo"); ?></a></section>';

                return html;
            }
    </script>
</head>
<body>
<!--PRELOADER-->
<div id="preloader">
    <div id="status">

    </div>
</div>
<!--/.PRELOADER END-->

<!--HEADER -->
<div class="header">
    <div class="for-sticky">
        <!--LOGO-->
        <div class="col-md-2 col-xs-6 logo">

            <module type="logo" image="<?php print template_url(); ?>images/logo.png" size="45">
        </div>
        <!--/.LOGO END-->
    </div>
    <div class="menu-wrap">
        <nav class="menu">
            <div class="menu-list">
                <module type="menu" template="bodo" id="main-menu">
            </div>
        </nav>
        <button class="close-button" id="close-button">Close Menu</button>
    </div>
    <module type="shop/cart" template="bodo_header" id="header-cart">
        <button class="menu-button" id="open-button">
            <span></span>
            <span></span>
            <span></span>
        </button><!--/.for-sticky-->

</div>
<!--/.HEADER END-->

<!--CONTENT WRAP-->
<div class="content-wrap">
    <!--CONTENT-->
    <div class="content">

